using System.Text.Json.Serialization;

namespace ServerList.Data;

public record Mod(string id, string version)
{
    [JsonPropertyName("id")] public string id { get; set; } = id;
    [JsonPropertyName("version")] public string version { get; set; } = version;
}