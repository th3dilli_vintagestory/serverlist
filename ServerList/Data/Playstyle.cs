using System.Text.Json.Serialization;

namespace ServerList.Data;

public record Playstyle(string id, string langCode)
{
    [JsonPropertyName("id")]
    public string id { get; set; } = id;

    [JsonPropertyName("langCode")]
    public string langCode { get; set; } = langCode;
}
