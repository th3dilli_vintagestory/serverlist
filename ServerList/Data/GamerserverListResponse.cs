using System.Text.Json.Serialization;

namespace ServerList.Data;

public record GamerserverListResponse(string status, List<Gameserver> data)
{
    [JsonPropertyName("status")] 
    public string status { get; set; } = status;
    [JsonPropertyName("data")] 
    public List<Gameserver> data { get; set; } = data;
}