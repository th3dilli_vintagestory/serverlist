using System.Text.Json;
using System.Text.Json.Serialization;
using Semver;

namespace ServerList.Data;

public record Gameserver(
    string ServerName,
    string ServerIp,
    Playstyle Playstyle,
    List<Mod> Mods,
    int MaxPlayers,
    int Players,
    SemVersion GameVersion,
    bool HasPassword,
    bool Whitelisted,
    string GameDescription)
{
    [JsonPropertyName("serverName")]
    public string ServerName { get; set; } = ServerName;

    [JsonPropertyName("serverIP")]
    public string ServerIp { get; set; } = ServerIp;

    [JsonPropertyName("playstyle")]
    public Playstyle Playstyle { get; set; } = Playstyle;

    [JsonPropertyName("mods")]
    public List<Mod> Mods { get; set; } = Mods;

    [JsonPropertyName("maxPlayers")]
    [JsonConverter(typeof(MaxPlayersConverter))]
    public int MaxPlayers { get; set; } = MaxPlayers;

    [JsonPropertyName("players")]
    public int Players { get; set; } = Players;

    [JsonPropertyName("gameVersion")]
    [JsonConverter(typeof(GameVersionConverter))]
    public SemVersion GameVersion { get; set; } = GameVersion;

    [JsonPropertyName("hasPassword")]
    public bool HasPassword { get; set; } = HasPassword;

    [JsonPropertyName("whitelisted")]
    public bool Whitelisted { get; set; } = Whitelisted;

    [JsonPropertyName("gameDescription")]
    public string GameDescription { get; set; } = GameDescription;
}

public class GameVersionConverter : JsonConverter<SemVersion>
{
    public override SemVersion Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        if (reader.TokenType == JsonTokenType.String)
        {
            return SemVersion.Parse(reader.GetString(), SemVersionStyles.Any);
        }

        return "0.0.0";
    }

    public override void Write(Utf8JsonWriter writer, SemVersion value, JsonSerializerOptions options)
    {
        writer.WriteStringValue(value.ToString());
    }
}

public class MaxPlayersConverter : JsonConverter<int>
{
    public override int Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        if (reader.TokenType == JsonTokenType.String)
        {
            if (int.TryParse(reader.GetString(), out int result))
            {
                return result;
            }
        }

        return 0;
    }

    public override void Write(Utf8JsonWriter writer, int value, JsonSerializerOptions options)
    {
        writer.WriteNumberValue(value);
    }
}
