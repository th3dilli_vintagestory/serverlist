using System.Text.Json;
using Quartz;
using ServerList.Data;

namespace ServerList;

public class GameserverService : IJob
{
    private readonly List<Gameserver> _gameservers = new();

    private readonly IHttpClientFactory _httpClientFactory;
    private DateTime _lastUpdateTimestamp;
    private readonly IServiceScopeFactory _scopeFactory;

    public GameserverService(IHttpClientFactory httpClientFactory, IServiceScopeFactory scopeFactory)
    {
        _httpClientFactory = httpClientFactory;
        _scopeFactory = scopeFactory;
        using var scope = _scopeFactory.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<DatabaseContext>();
        dbContext.Database.EnsureCreatedAsync();
    }

    public static void QuarzConfigure(IServiceCollectionQuartzConfigurator q)
    {
#if !DEBUG
        var jobkey = new JobKey("ConconcurrentJob");
        q.AddJob<GameserverService>(opts => opts.WithIdentity(jobkey));
        q.AddTrigger(opts => opts
            .ForJob(jobkey)
            .WithIdentity("trigger1", "group1")
            .WithCronSchedule("0 0/15 * * * ?"));
#endif
    }

    public async Task<List<Gameserver>> GetGameServerList(CancellationToken cancellationToken = new() , bool shouldUpdate = false)
    {
        if (!shouldUpdate)
        {
            lock (_gameservers)
            {
                if (_gameservers.Count == 0 || DateTime.UtcNow - _lastUpdateTimestamp >= TimeSpan.FromMinutes(5))
                {
                    shouldUpdate = true;
                }
            }
        }

        if (shouldUpdate)
        {
#if DEBUG
            var json = await File.ReadAllTextAsync("list.json");
            var dataJson = JsonSerializer.Deserialize<GamerserverListResponse>(json);
            if (dataJson?.data != null)
            {
                lock (_gameservers)
                {
                    _gameservers.Clear();
                    _gameservers.AddRange(dataJson.data);
                }
            }
#else
            var downloadServerList = await DownloadServerList(cancellationToken);
            lock (_gameservers)
            {
                _gameservers.Clear();
                _gameservers.AddRange(downloadServerList);
            }
#endif
        }

        lock (_gameservers)
        {
            return _gameservers;
        }
    }

    private async Task<List<Gameserver>> DownloadServerList(CancellationToken cancellationToken)
    {
        var httpClient = _httpClientFactory.CreateClient();
        httpClient.Timeout = TimeSpan.FromSeconds(10);
        try
        {
            var response = await httpClient.GetAsync("http://masterserver.vintagestory.at/api/v1/servers/list", cancellationToken);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync(cancellationToken);
                var data = JsonSerializer.Deserialize<GamerserverListResponse>(content);
                if (data?.status.Equals("ok") == true)
                {
                    _lastUpdateTimestamp = DateTime.UtcNow;
                    Console.WriteLine($"Updated Server list: {_lastUpdateTimestamp}");
                    return data.data;
                }

                return [];
            }

            Console.WriteLine($"API request failed with status code {response.StatusCode}");
        }
        catch (Exception e)
        {
            Console.WriteLine($"Failed getting server list: {e.Message}");
        }
        return [];
    }

    public async Task Execute(IJobExecutionContext context)
    {
        try
        {
            await FetchData(context.CancellationToken);
        }
        catch (Exception e)
        {
            Console.WriteLine($"Cronjob Failed: {e.Message}");
        }
    }

    public async Task FetchData(CancellationToken cancellationToken)
    {
        using var scope = _scopeFactory.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<DatabaseContext>();

        var now = DateTime.UtcNow;
        var downloadServerList = (await GetGameServerList(cancellationToken, true)).ToDictionary(k => k.ServerIp, g => g);

        foreach (var server in dbContext.Gameserver)
        {
            var s = downloadServerList.Values.FirstOrDefault(g => g.ServerIp.Equals(server.ServerIp));
            if (s == null) continue;

            server.Status.Add(new GameServerStatusDb(server, s.Players, now));
            server.ServerName = s.ServerName;
            server.GameVersion = s.GameVersion.ToString();
            server.MaxPlayers = s.MaxPlayers;
            server.HasPassword = s.HasPassword;
            server.Whitelisted = s.Whitelisted;
            server.GameDescription = s.GameDescription;
            server.Mods = JsonSerializer.Serialize(s.Mods);
            server.PlayStyle = s.Playstyle.langCode;
            downloadServerList.Remove(s.ServerIp);
        }

        var gameServerStatusDbs = new List<GameServerDb>();
        foreach (var gm in downloadServerList.Values)
        {
            var gmd = new GameServerDb(
                gm.ServerIp,
                gm.ServerName,
                gm.GameVersion.ToString(),
                gm.MaxPlayers,
                gm.HasPassword,
                gm.Whitelisted,
                gm.GameDescription,
                JsonSerializer.Serialize(gm.Mods),
                gm.Playstyle.langCode
            );
            gmd.Status.Add(new GameServerStatusDb(gmd, gm.Players, now));
            gameServerStatusDbs.Add(gmd);
        }

        dbContext.Gameserver.AddRange(gameServerStatusDbs);

        await dbContext.SaveChangesAsync(cancellationToken);
    }
}