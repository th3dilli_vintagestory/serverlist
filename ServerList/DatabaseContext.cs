using Microsoft.EntityFrameworkCore;

namespace ServerList;

public class DatabaseContext : DbContext
{
    public DbSet<GameServerDb> Gameserver { get; set; }
    public DbSet<GameServerStatusDb> GameserverStatus { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        => optionsBuilder.UseSqlite("Data Source=serverlist_db.dat");
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<GameServerDb>()
            .HasMany(e => e.Status)
            .WithOne(e => e.Server)
            .HasForeignKey(e => e.ServerId);
    }
}

