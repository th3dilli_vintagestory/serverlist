using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace ServerList;

public record GameServerDb(
    string ServerIp,
    string ServerName,
    string GameVersion,
    int MaxPlayers,
    bool HasPassword,
    bool Whitelisted,
    string GameDescription,
    string Mods,
    string PlayStyle
)
{
    [Key] 
    public ulong ServerId { get; init; }
    public string ServerIp { get; set; } = ServerIp;
    
    public string ServerName {get; set; } = ServerName;
    public string GameVersion {get; set; } = GameVersion;
    public int MaxPlayers {get; set; } = MaxPlayers;
    public bool HasPassword {get; set; } = HasPassword;
    public bool Whitelisted {get; set; } = Whitelisted;
    public string GameDescription {get; set; } = GameDescription;
    public string Mods {get; set; } = Mods;
    public string PlayStyle {get; set; } = PlayStyle;

    public ICollection<GameServerStatusDb> Status { get; set; } = [];
}

[PrimaryKey(nameof(ServerId), nameof(Timestamp))]
public record GameServerStatusDb(
    GameServerDb Server,
    int Players,
    DateTime Timestamp)
{
    public GameServerStatusDb(
        int players,
        DateTime timestamp) : this(null, 
        players,
        timestamp)
    {
       
        Players = players;
        Timestamp = timestamp;
    }

    public ulong ServerId { get; set; }

    public GameServerDb Server { get; set; } = Server;
    public int Players { get; set; } = Players;
    public DateTime Timestamp { get; set; } = Timestamp;
}