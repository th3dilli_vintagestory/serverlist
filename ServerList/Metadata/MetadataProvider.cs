namespace ServerList.Metadata;

public class MetadataProvider
{
    public Dictionary<string, MetadataValue> RouteDetailMapping { get; set; } = new()
    {
        {
            "/",
            new()
            {
                Title = "Vintage Story Server List and Stats page",
                Description = "Visit vintagestory.at for more information about the game."
            }
        },
        {
            "/about",
            new()
            {
                Title = "About us",
                Description = "Email us: th3dillidev@gmail.com - Th3Dilli"
            }
        },
        {
            "/list",
            new()
            {
                Title = "Vintage Story Server List",
                Description = "Brows through a filterable server list with a variety of filters"
            }
        },
        {
            "/serverdetail",
            new()
            {
                Title = "Server Details",
                Description = "Shows detailed information about a server"
            }
        },
        {
            "/stats",
            new()
            {
                Title = "Server and Mod Stats",
                Description = "Shows statistics about servers. Lists versions and mod usage per server."
            }
        }
    };
}

public class MetadataValue
{
    public string Title { get; set; }
    public string Description { get; set; }
}