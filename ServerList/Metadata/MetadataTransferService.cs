using System.ComponentModel;
using System.Runtime.CompilerServices;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Routing;

namespace ServerList.Metadata;

public class MetadataTransferService : INotifyPropertyChanged, IDisposable
{
    public event PropertyChangedEventHandler PropertyChanged;

    private string _title;

    public string Title
    {
        get => _title;
        set
        {
            _title = value;
            OnPropertyChanged();
        }
    }

    private string _description;
    private readonly NavigationManager _navigationManager;
    private readonly MetadataProvider _metadataProvider;

    public string Description
    {
        get => _description;
        set
        {
            _description = value;
            OnPropertyChanged();
        }
    }

    public void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
        PropertyChanged?.Invoke(this, new(propertyName));
    }

    public MetadataTransferService(NavigationManager navigationManager, MetadataProvider metadataProvider)
    {
        _navigationManager = navigationManager;
        _metadataProvider = metadataProvider;
        _navigationManager.LocationChanged += UpdateMetadata;
        UpdateMetadata(_navigationManager.Uri);
    }

    private void UpdateMetadata(object? sender, LocationChangedEventArgs e)
    {
        UpdateMetadata(e.Location);
    }

    public void Dispose()
    {
        _navigationManager.LocationChanged -= UpdateMetadata;
    }

    private void UpdateMetadata(string url)
    {
        var uri = new Uri(url);
        var last = "/"+uri.Segments.Last();
        var metadataValue = _metadataProvider.RouteDetailMapping.FirstOrDefault(vp => last.Equals(vp.Key)).Value;

        if (metadataValue is null)
        {
            metadataValue = new()
            {
                Title = "Default",
                Description = "Default"
            };
        }

        Title = metadataValue.Title;
        Description = metadataValue.Description;
    }
}