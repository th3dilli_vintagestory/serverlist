using MudBlazor.Services;
using Quartz;
using ServerList;
using ServerList.Metadata;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();
builder.Services.AddMudServices();

builder.Services.AddHttpClient();
builder.Services.AddDbContextFactory<DatabaseContext>();

builder.Services.AddSingleton<GameserverService>();
builder.Services.AddSingleton<MetadataProvider>();
builder.Services.AddScoped<MetadataTransferService>();

builder.Services.AddQuartz(GameserverService.QuarzConfigure);
builder.Services.AddQuartzHostedService(q => q.WaitForJobsToComplete = true);

var app = builder.Build();

if (!app.Environment.IsDevelopment())
{
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseStaticFiles();

app.UseRouting();

app.MapBlazorHub();
app.MapFallbackToPage("/_Host");

// var service = app.Services.GetService<GameserverService>();
// await service?.FetchData(new CancellationTokenSource().Token)!;
app.Run();